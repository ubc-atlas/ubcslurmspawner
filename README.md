# UBCSlurmSpawner

## Installation
Installation is handled via pip, as both wrapspawner and batchspawner are available on PyPI. For an editable version, run
```console
$ pip install -e .
```
from the root directory of this repository.

## Usage
Edit `jupyterhub_config.py` to tell JupyterHub to use the `UBCSlurmSpawner` class. Slurm `sbatch` options can be either statically configured via `BatchSpawnerBase` or set to be user-configured via `UBCSlurmSpawner.available_options`.
```python
import ubcslurmspawner
c.JupyterHub.spawner_class = 'ubcslurmspawner.UBCSlurmSpawner'
c.BatchSpawnerBase.req_prologue = 'source /etc/profile.d/conda.sh ; conda activate base'
```
