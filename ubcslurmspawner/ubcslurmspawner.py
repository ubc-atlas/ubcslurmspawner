from traitlets import Unicode
import wrapspawner
import batchspawner

class UBCSlurmSpawner(wrapspawner.WrapSpawner):

    available_options = [
        ('req_nprocs', 'CPUs', [2, 4, 8, 12, 16, 24, 32], lambda x: x),
        ('req_memory', 'RAM (GB)', [4, 8, 12, 16, 24, 32, 64, 96], lambda x: str(x)+'gb'),
        ('req_runtime', 'Time (hr)', [2, 4, 8, 12, 24, 48, 120], lambda x: str(x)+':00:00'),
        ]

    select_template = Unicode(
        """
        <label for="{name}">{display}</label>
        <select name="{name}" class="form-control" required autofocus>
            {options_list}
        </select>
        """,
        config=True,
        help = """Template for each set of selections in options_form."""
        )

    first_template = Unicode('selected',
        config=True,
        help="Text to substitute as {first} in input_template"
        )
    
    input_template = Unicode("""
        <option value="{key}" {first}>{display}</option>""",
        config = True,
        help = """Template to construct {input_template} in form_template. This text will be formatted
            against each item in the profiles list, in order, using the following key names:
            ( display, key, type ) for the first three items in the tuple, and additionally
            first = "checked" (taken from first_template) for the first item in the list, so that
            the first item starts selected."""
        )

    options_form = Unicode()

    def _options_form_default(self):
        form_text = []
        for section in self.available_options:
            sec_name, sec_disp, sec_list, sec_func = section
            temp_keys = [ dict(display=val, key=sec_func(val), first='') for val in sec_list ]
            temp_keys[0]['first'] = self.first_template
            options_text = ''.join([ self.input_template.format(**tk) for tk in temp_keys ])
            form_text.append(self.select_template.format(name=sec_name, display=sec_disp, options_list=options_text))
        form_text.append('<label for="req_options">Extra sbatch Options:</label>')
        form_text.append('<textarea class="form-control" name="req_options" id="req_options" placeholder="e.g. --nodelist=atlasserv5"></textarea>')
        return ''.join(t for t in form_text)

    def options_from_form(self, formdata):
        options = {}
        for section in self.available_options:
            sec_name = section[0]
            options[sec_name] = formdata.get(sec_name)[0]
        options['req_options'] = formdata.get('req_options')[0]
        return options

    def construct_child(self):
        self.child_config = self.user_options
        self.child_class = batchspawner.SlurmSpawner
        super().construct_child()

